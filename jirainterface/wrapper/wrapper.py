from GoogleDriveInterface import gdrive_interface
from JiraInterface import jira_to_excel
import openpyxl
import os
import shutil


class Wrapper:

    path_to_reports = None
    GoogleDrive = None
    GoogleDrive_reports_folder = None

    def __init__(self):
        self.GoogleDrive_reports_folder = '104f6TIRZnCEQyj1XnCTd5zAXPhfAeiOJ'
        self.GoogleDrive = gdrive_interface.GoogleDriveInterface()
        self.path_to_reports = '../Reports/'

    def run(self, sprint_id):
        """ Runs the main program.

            * Check if there is a backup directory
                * If not, create
            * Check if there is a directory for the sprint on Google Drive
                * If not, create
            * Check if there is an excel file for the sprint on Google Drive
                * If not, create
            * Download excel sheet from Google Drive to local path
            * Update the downloaded excel with data from JIRA.
            * Replace excel-book on Google Drive with the one updated from JIRA (local)

        :param sprint_id: ID of the sprint (i.e 3 for 'RTS-3')
        """
        print('Checking if backup dir exists')
        if not os.path.isdir('../Reports/Backup'):
            print('Backup dir not found, creating dir')
            os.mkdir('../Reports/Backup/')
        else:
            print('Backup dir found')
        folder_name = 'RTS-' + str(sprint_id)
        file_name = 'SprintRapport_RTS-' + str(sprint_id)
        print('Checking if dir for RTS-' + str(sprint_id) + ' exists on Google Drive')
        folder_id = self.GoogleDrive.check_if_exists_drive(self.GoogleDrive_reports_folder, folder_name)
        while folder_id is None:
            print('Dir not found, creating folder for RTS-' + sprint_id)
            self.GoogleDrive.create_folder(self.GoogleDrive_reports_folder, folder_name)
            print('Dir created')
            folder_id = self.GoogleDrive.check_if_exists_drive(self.GoogleDrive_reports_folder, folder_name)
        print('Dir found')
        self.GoogleDrive.create_excel_on_drive(folder_id, sprint_id)
        drive_file_id = self.GoogleDrive.check_if_exists_drive(folder_id, file_name)
        self.GoogleDrive.download_excel_from_drive(drive_file_id, sprint_id)
        self.update_local_excel(sprint_id)
        self.GoogleDrive.update_excel_on_drive(folder_id, sprint_id)

    def check_if_report_exists(self, sprint_id):
        """ Check if a report for the sprint ID exists locally

        :param sprint_id: ID of the sprint (i.e 3 for 'RTS-3')
        :return: 'True' if a report is found
        :return: 'False' if a report is not found
        """
        print("Checking if report for 'RTS-" + sprint_id + "' exists ...")
        if os.path.isfile('../Reports/SprintRapport_RTS-' + sprint_id + '.xlsx'):
            print("Found report for 'RTS-" + sprint_id + "'")
            return True
        else:
            print("Couldn't find report for 'RTS-" + sprint_id + "', generating report ...")
            return False

    def create_excel_local(self, sprint_id):
        """ Create an excel-file locally to store sprint-data.
            Copies the template created for reports and pastes it to the destination directory.
            Renames the .xlsx file to comply with the sprint-id given as param.

        :param sprint_id:
        """
        old_name = 'SprintRapport_Mal.xlsx'
        new_name = 'SprintRapport_RTS-' + sprint_id + '.xlsx'
        source_file = '../Reports/Mal/' + old_name
        dest_dir = '../Reports/'
        shutil.copy(source_file, dest_dir)
        dest_file = os.path.join(dest_dir, old_name)
        new_dest_file_name = os.path.join(dest_dir, new_name)
        os.rename(dest_file, new_dest_file_name)

    def update_local_excel(self, sprint_id):
        """ Updates the report stored locally.

        :param sprint_id: ID of the sprint (i.e 3 for 'RTS-3')
        """
        jira = jira_to_excel.JiraToExcel(self.path_to_reports, sprint_id)
        jira.generate_plan()
        jira.generate_review()


wrapper = Wrapper()
wrapper.run(4)
