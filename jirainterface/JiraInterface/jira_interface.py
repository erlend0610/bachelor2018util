from jira import JIRA
from JiraInterface import jira_issue as ji, credentials
import pprint


class JiraInterface:
    options = None
    jira = None
    search = None
    jira_issue = ji.JiraIssue()
    sanitized_issue = None
    sanitized_response = list()
    raw_request = None

    def __init__(self, sprint_id):
        self.options = {
            'server': 'https://robotrim2018.atlassian.net'
        }
        print('Connecting to JIRA-domain: ' + self.options['server'])
        self.jira = JIRA(self.options, basic_auth=(credentials.username, credentials.password))
        print('Connection established!')
        print('Searching JIRA for sprint with ID: ' + sprint_id)
        self.search = "sprint=" + str(sprint_id)
        print('Fetching JSON-response from JIRA')
        self.raw_request = self.jira.search_issues(self.search, maxResults=100, json_result=True)
        # print(jira.search_issues(self.search, s)

    # Function to sanitize JSON-data gotten from the request to JIRA.
    def sanitize_response(self):
        """ Sanitize raw JSON-response from JIRA

        :return: Sanitized response in the form of dict
        """
        self.sanitized_response = list()
        print('Sanitizing raw response from JIRA')
        for raw_issue in self.raw_request['issues']:
            parent = None
            if raw_issue['fields']['issuetype']['subtask'] is True:
                parent = raw_issue['fields']['parent']['key']
            key = raw_issue['key']
            assignee = self.sanitize_assignee(raw_issue['fields']['assignee'])
            summary = raw_issue['fields']['summary']
            status = raw_issue['fields']['status']['name']
            issuetype, estimate = self.sanitize_estimate(raw_issue)
            self.sanitized_issue = self.jira_issue.create_issue(key, assignee,
                                                                summary, issuetype, status, estimate, parent)
            self.sanitized_response.append(self.sanitized_issue)
        print('Finished sanitizing response, returning sanitized response')
        return self.sanitized_response

    # Function to sanitize the raw assignee JSON-data
    def sanitize_assignee(self, raw_assignee):
        """ Sanitize assignee field of raw JSON-response from JIRA

        :param raw_assignee: Raw assignee containing all information from JIRA
        :return: Sanitized assignee response
        """
        if raw_assignee is not None:
            response = raw_assignee['name']
        else:
            response = None
        return response

    # Function to sanitize the raw JSON-data and return type of issue and estimate
    # Will return 'None' if the task is not of type 'Story'
    def sanitize_estimate(self, raw_issue):
        """ Sanitize estimate field of raw JSON-response from JIRA

        :param raw_issue: Raw issue containing all information from JIRA
        :return: Sanitized estimate response
        """
        name = raw_issue['fields']['issuetype']['name']
        if name == "Story":
            return name, raw_issue['fields']['customfield_10014']
        else:
            return name, None
