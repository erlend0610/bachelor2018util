import openpyxl
from JiraInterface import jira_interface


class JiraToExcel:
    id_nr = 3
    sprint_id = None
    next_sprint_id = None
    jira = None
    next_jira = None
    wb = None
    path = None
    filename = None

    def __init__(self, path, sprint_id):
        self.sprint_id = 'RTS-' + str(sprint_id)
        self.next_sprint_id = 'RTS-' + str(sprint_id + 1)
        self.jira = jira_interface.JiraInterface(self.sprint_id)
        try:
            self.next_jira = jira_interface.JiraInterface(self.next_sprint_id)
        except:
            print('Could not find data for next sprint.')
            pass

        self.path = path
        self.filename = "SprintRapport_" + self.sprint_id + ".xlsx"

    def load_workbook(self):
        """ Loads an excel-workbook
        """
        print('Loading workbook: ' + self.filename)
        return openpyxl.load_workbook(self.path + self.filename)

    def generate_plan(self):
        """ Generate a ready-to-export plan from JIRA-response

        """
        print('Generating plan from JIRA-response')
        response = self.jira.sanitize_response()
        self.export_plan_to_excel(response, 'SprintPlan')
        if self.next_jira is not None:
            print('Generating plan for next sprint')
            response = self.next_jira.sanitize_response()
            self.export_plan_to_excel(response, 'SprintPlanNext')

    def export_plan_to_excel(self, response, sheet_name):
        """ Export a sanitized plan to excel

        :param response: Sanitized response from JIRA
        :param sheet_name: Name of the sheet to export to
        """
        print('Exporting ' + sheet_name + ' from JIRA to Excel')
        book = self.load_workbook()
        sheet = book.get_sheet_by_name(sheet_name)
        row = 5
        if sheet['H1'] == 'Marked':
            print('Sheet already updated, moving on')
            pass
        else:
            for resp in response:
                if resp['parent'] is not None:
                    sheet['A' + str(row)] = resp['parent'] + ' / ' + resp['key']
                else:
                    sheet['A' + str(row)] = resp['key']
                sheet['B' + str(row)] = resp['summary']
                name = self.get_name(resp['assignee'])
                sheet['C' + str(row)] = name
                sheet['D' + str(row)] = resp['estimate']
                row = row + 1
            sheet['H1'] = "Marked"
            book.save(self.path + self.filename)

    def generate_review(self):
        """ Generate a ready-to-export review from JIRA-response

        """
        print('Generating review from JIRA-response')
        response = self.jira.sanitize_response()
        book = self.load_workbook()
        sheet = book.get_sheet_by_name("SprintReview")
        row = 5
        for resp in response:
            if resp['parent'] is not None:
                sheet['A' + str(row)] = resp['parent'] + ' / ' + resp['key']
            else:
                sheet['A' + str(row)] = resp['key']
            name = self.get_name(resp['assignee'])
            sheet['B' + str(row)] = name
            sheet['C' + str(row)] = resp['estimate']
            status = self.check_status(resp['status'])
            sheet['D' + str(row)] = status
            row = row + 1
        book.save(self.path + self.filename)

    def check_status(self, status):
        """ Check status of tasks in backlog

        :param status: Status in JIRA-form
        :return: Status in form which is used in Excel-sheet
        """
        if status == "Done":
            return "F"
        elif status == "In Progress" or status == "Closed" or status == "To Do":
            return "IF"

    def get_name(self, assignee):
        """ Interprets usernames from JIRA

        :param assignee: Usernames from JIRA
        :return: Converted names in form of initials
        """
        names = {
            'admin': 'EH',
            'oleafr': 'OAFR',
            'soli-aro': 'SLA',
            'thosava': 'TSV',
            'peranders.stadheim': 'PAS',
            'oyvindaaslie': 'ØJCÅ'
        }
        if assignee is None or assignee == "":
            return ""
        else:
            return names[assignee]



