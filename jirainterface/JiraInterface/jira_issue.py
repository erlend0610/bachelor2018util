class JiraIssue:
    issue = {
        'key': None,
        'assignee': None,
        'summary': None,
        'issuetype': None,
        'status': None,
        'estimate': None,
        'parent': None
    }

    def create_issue(self, key, assignee, summary, issuetype, status, estimate, parent):
        self.issue = {
            'key': key,
            'assignee': assignee,
            'summary': summary,
            'issuetype': issuetype,
            'status': status,
            'estimate': estimate,
            'parent': parent
        }
        return self.issue
