from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
import os
import shutil

#   Folder IDs:
#
#   Bacheloroppgave2018: 1sjdtzE4UahVuo8ewvHzypHuD2oU-hs2X
#       SprintRapporter: 104f6TIRZnCEQyj1XnCTd5zAXPhfAeiOJ
#       Dokumentasjon: 1u7z7UGb6YRTSQqaeKqxpkkAm0RAeMpwU
#


class GoogleDriveInterface:

    gauth = None
    drive = None
    sprint_id = None
    file_list = None
    local_path = None

    def __init__(self):
        self.gauth = GoogleAuth()
        self.gauth.LocalWebserverAuth()
        self.drive = GoogleDrive(self.gauth)

    def check_if_exists_drive(self, parent_id, name):
        """ Check if a file exists on Google Drive

        :param parent_id: ID of the folder to search
        :param name: Name of the folder to find
        :return: GoogleDriveFile
        """
        print('Checking if file ' + name + ' exists ...')
        self.file_list = self.drive.ListFile({
            'q': '"{}" in parents and title="{}" and trashed=false'.format(parent_id, name)
        }).GetList()
        if self.file_list.__len__() <= 0:
            print('Could not find file ' + name)
            return None
        else:
            print('Found file ' + self.file_list[0]['title'])
            return self.file_list[0]['id']

    def create_folder(self, parent_id, name):
        """ Create a folder on Google Drive

        :param parent_id: ID of the parent folder (folder in which to create the file)
        :param name: The name to be stored to the file
        """
        folder_metadata = {
            'parents': [{
                'id': parent_id
            }],
            'title': name,
            'mimeType': 'application/vnd.google-apps.folder'
        }
        folder = self.drive.CreateFile(folder_metadata)
        folder.Upload()

    def create_excel_on_drive(self, parent_id, sprint_id):
        """ Create a excel-sheet on Google Drive, converts to Google Sheets.
            Currently needs the excel file to be stored in the same dir as pyfile.

        :param parent_id: ID of the folder to store the excel file on GD
        :param sprint_id: The ID of the sprint (i.e 3 for 'RTS-3)
        """
        sprint_id = str(sprint_id)
        file_name = 'SprintRapport_RTS-' + sprint_id
        print('Check if ' + file_name + ' exists on Google Drive')
        if self.check_if_exists_drive(parent_id, file_name) is None:
            print(file_name + ' not found in, creating file')
            file = self.drive.CreateFile({
                'parents': [{
                    'kind': 'drive#fileLink',
                    'id': parent_id
                }],
                'title': 'SprintRapport_RTS-' + sprint_id
            })
            content_file = '../SprintRapport_Mal.xlsx'
            print('Setting content file to ' + content_file)
            file.SetContentFile(content_file)
            file.Upload({
                'convert': True
            })
            print(file_name + ' created ...')
        else:
            print(file_name + ' exists')

    def update_excel_on_drive(self, parent_id, sprint_id):
        """ Update an excel file for the given sprint on Google Drive

        :param parent_id: ID of the parent folder
        :param sprint_id: ID of the sprint (i.e 3 for RTS-3)
        """
        print('Updating data on Google Drive')
        sprint_id = str(sprint_id)
        file_name = 'SprintRapport_RTS-' + sprint_id
        id = self.check_if_exists_drive(parent_id, file_name)
        file = self.drive.CreateFile({
            'id': id
        })
        content_file = os.path.dirname(os.getcwd()) + '/Reports/SprintRapport_RTS-' + sprint_id + '.xlsx'
        print('Setting content file to ' + content_file)
        file.SetContentFile(content_file)
        file.Upload()
        print('File updated!')

    def download_excel_from_drive(self, file_id, sprint_id):
        """ Download excel-file from Google Drive

        :param file_id: GoogleDriveFile-ID of the file to download
        :param sprint_id: ID of the sprint (i.e 3 for RTS-3)
        """
        sprint_id = str(sprint_id)
        path = os.path.dirname(os.getcwd()) + '/Reports/SprintRapport_RTS-' + sprint_id + '.xlsx'
        print('Checking if file exists locally at: ' + path)
        excel_file = self.drive.CreateFile({
            'id': file_id
        })
        if not os.path.isfile(path):
            print('File not found, downloading from Google Drive')
            excel_file.GetContentFile(path, mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            print('File downloaded to: ' + path)
        else:
            print('File found at: ' + path)
            backup_path = os.path.dirname(os.getcwd()) + '/Reports/Backup/'
            self.backup_and_update_local_file(path, backup_path, excel_file)

    def backup_and_update_local_file(self, source_file, dest_dir, file):
        """ Backup the old source file to Backup folder, delete the original file
            and download an updated file to Reports dir.

        :param source_file:
        :param dest_dir:
        :param file:
        """
        print('Taking backup of file, storing in ' + dest_dir)
        shutil.copy(source_file, dest_dir)
        print('Deleting original file')
        os.remove(source_file)
        print('Deleted')
        file.GetContentFile(source_file, mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        print('Updated file downloaded to ' + source_file)


