

class LatexInput:
    sprint_id = None
    start_date = None
    end_date = None

    report_preamble = None
    plan = None
    review = None
    retro = None
    individual_time = None

    def __init__(self, sprint_id):
        self.sprint_id = str(sprint_id)

    def create_report_preamble(self, start_date, end_date):
        """
        Generates the latex-code for the report-preamble

        :param start_date: Start date of the sprint
        :param end_date: End date of the sprint
        :return: String containing latex code for report preamble
        """
        report = "\\documentclass[11pt,a4paper]{article}\n" \
                 "\\pagestyle{headings}\n" \
                 "\\usepackage[utf8]{inputenc}\n" \
                 "\\usepackage{amsmath}\n" \
                 "\\usepackage{amsfonts}\n" \
                 "\\usepackage{amssymb}\n" \
                 "\\usepackage{graphicx, wrapfig}\n" \
                 "\\usepackage{float}\n" \
                 "\\usepackage[export]{adjustbox}\n" \
                 "\\usepackage{multirow}\n" \
                 "\\usepackage{booktabs} % For prettier tables\n" \
                 "\\usepackage{tabularx}\n" \
                 "\\usepackage{longtable}\n" \
                 "\\usepackage[explicit]{titlesec}\n" \
                 "\\usepackage{array}\n" \
                 "\\usepackage[width=16.00cm, left=2.50cm, top=2.5cm, bottom=4cm]{geometry}\n" \
                 "\\usepackage{fancyhdr}\n" \
                 "\\pagestyle{fancy}\n" \
                 "\\fancyhf{}\n" \
                 "\\lhead{Dokument ID: SR-" + self.sprint_id + "-EH}\n" \
                                                               "\\rhead{Bachelor: HS-RoboTrim2018}\n" \
                                                               "\\cfoot{\n" \
                                                               "\\thepage\n" \
                                                               "}\n" \
                                                               "\\begin{document}\n" \
                                                               "\\pagenumbering{gobble}\n" \
                                                               "\\begin{titlepage}\n" \
                                                               "\\centering\n" \
                                                               "{\\scshape\LARGE HS-RoboTrim2018 \par}\n" \
                                                               "\\vspace{1cm}\n" \
                                                               "{\\scshape\\Large Oppfølgingsdokument/Sprint-rapport\par}\n" \
                                                               "\\vspace{1.5cm}\n" \
                                                               "{\\scshape\\bfseries}" + "RTS-" + self.sprint_id + "\n" \
                                                                                                       "\\vspace{2cm}\\par\n" \
                                                                                                       "{\\Large\\itshape Sondre L. Aronsen\\\\Per A. Stadheim\\\\Ole Anders F. Reistad\\\\\n" \
                                                                                                       "Thomas S. Vatle\\\\Øyvind J. Åslie\\\\Erlend Helgerud \par}\n" \
                                                                                                       "\\vfill\n" \
                                                                                                       "% Bottom of the page\n" \
                                                                                                       "{\\large \\today\par}\n" \
                                                                                                       "\\end{titlepage}\n" \
                                                                                                       "\\clearpage\n" \
                                                                                                       "\\pagenumbering{arabic}\n" \
                                                                                                       "\\bgroup\n" \
                                                                                                       "\\def\\arraystretch{2}\n" \
                                                                                                       "\\noindent\n" \
                                                                                                       "\\begin{tabularx}{\\textwidth}{p{0.32\\textwidth} p{0.32\\textwidth} p{0.32\\textwidth}}\n" \
                                                                                                       "\\hline\n" \
                                                                                                       "\\textbf{Sprint ID:} RTS-" + self.sprint_id + "& \\textbf{Startdato: }" + str(start_date) + \
                 "& \\textbf{Sluttdato: } " + end_date + "\\\\\n" \
                                                                   "\\hline\n" \
                                                                   "\\end{tabularx}\\bigskip\n" \
                                                                   "\\egroup\n" \
                                                                   "\n\\noindent Dette dokumentet inneholder rapporten av foregående sprint, som består av:\n" \
                                                                   "\\begin{itemize}\n" \
                                                                   "\\item Sprint-planlegging\n" \
                                                                   "\\item Sprint-gjennomgang\n" \
                                                                   "\\item Sprint-retrospektiv\n" \
                                                                   "\\item Individuelle timelister\n" \
                                                                   "\\end{itemize}\\bigskip\n" \
                                                                   "\\noindent Forkortelser brukt for navn:\n" \
                                                                   "\\begin{itemize}\n" \
                                                                   "\\item \\textbf{SLA}: Sondre Lieblein Aronsen\n" \
                                                                   "\\item \\textbf{PAS}: Per Anders Stadheim\n" \
                                                                   "\\item \\textbf{OAFR}: Ole Anders Foss Reistad\n" \
                                                                   "\\item \\textbf{TSV}: Thomas Saraby Vatle\n" \
                                                                   "\\item \\textbf{ØJCÅ}: Øyvind Jia-Chen Åslie\n" \
                                                                   "\\item \\textbf{EH}: Erlend Helgerud\n" \
                                                                   "\\end{itemize}\\bigskip\n" \
                                                                   "Forkortelser brukt for saks-statuser:\n" \
                                                                   "\\begin{itemize}\n" \
                                                                   "\\item \\textbf{F}: Ferdig\n" \
                                                                   "\\item \\textbf{IF}: Ikke ferdig\n" \
                                                                   "\\item \\textbf{K}: Kansellert\n" \
                                                                   "\\end{itemize}\n"
        return report

    def create_plan(self, temp_input, date, goal, planned_issues, planned_estimate):
        """
        Generates the latex code for the sprint-plan

        :param temp_input: List containing data from excel-sheet generated from JIRA
        :param date: Start date of the sprint
        :param goal: Goal of the sprint
        :param planned_issues: Number of planned issues
        :param planned_estimate: Number indicating the estimated work points
        :return: A list containing the latex code for the plan
        """
        data = list()
        plan_start = "\\begin{titlepage}\n" \
                     "\\centering\n" \
                     "{\\scshape\\LARGE HS-RoboTrim2018 \\par}\n" \
                     "\\vspace{1cm}\n" \
                     "{\\scshape\\Large Sprintplanlegging \\par}\n" \
                     "Dette dokumentet viser hvordan prosjektgruppa planla den fullførte sprinten\n" \
                     "\\vfill\n" \
                     "\\end{titlepage}" \
                     "\\begin{center}\n" \
                     "\\bgroup\n" \
                     "\\def\\arraystretch{2}\n" \
                     "\\begin{table}[h!]\n" \
                     "\\begin{tabularx}{\\textwidth}{p{0.32\\textwidth} p{0.32\\textwidth} p{0.32\\textwidth}}\n" \
                     "\\hline\n" \
                     "\\textbf{Dato: } \n" \
                     + str(date) + \
                     " & & \\\\\n" \
                     "\\hline\n" \
                     "\\multicolumn{3}{l}{\n" \
                     "\\textbf{Sprintmål: }" + str(goal) + "} \\\\\n" \
                                                      "\\hline\n" \
                                                      "\\end{tabularx}\n" \
                                                      "\\end{table}" \
                                                      "\\begin{longtable}{p{0.20\\textwidth} p{0.40\\textwidth} p{0.15\\textwidth} p{0.25\\textwidth}}\n" \
                                                      "\\textbf{Saks-ID} & \\textbf{Navn} & \\textbf{Ansvarlig} & \\textbf{Estimat} \\\\\n" \
                                                      "\\hline\n"
        data.append(plan_start)
        for input in temp_input:
            data.append(input)
        plan_end = "\\end{longtable}\n" \
                   "\\begin{tabularx}{\\textwidth}{p{0.25\\textwidth} p{0.25\\textwidth} p{0.25\\textwidth} p{0.25\\textwidth}}\n" \
                   "\\textbf{Antall saker: }" \
                    + str(planned_issues) + \
                   " & & & \\\\\n" \
                   "\\textbf{Totalt estimat: }" \
                    + str(planned_estimate) + \
                   " & & & \\\\\n" \
                   "\\end{tabularx}\n" \
                   "\\egroup\n" \
                   "\\end{center}\n"
        data.append(plan_end)
        return data

    def create_review(self, temp_input, end_date, planned_issues, total_issues, incomplete_issues,
                      cancelled_issues, complete_issues, planned_estimate, total_estimate,
                      incomplete_estimate, cancelled_estimate, complete_estimate):
        """
        Generates the latex code for the sprint review

        :param temp_input: List containing data for the sprint-review from the excel sheet generated from JIRA data
        :param end_date: End date of the sprint
        :param planned_issues: Number of planned issues for the sprint
        :param total_issues: Total issues during the sprint
        :param incomplete_issues: Number of incomplete issues during the sprint
        :param cancelled_issues: Number of cancelled issues during the sprint
        :param complete_issues: Number of complete issues during the sprint
        :param planned_estimate: The estimated work points for the sprint
        :param total_estimate: The total amount of work points for the sprint
        :param incomplete_estimate: The number of incomplete work points during the sprint
        :param cancelled_estimate: The number of cancelled work points during the sprint
        :param complete_estimate: The number of complete work points during the sprint
        :return: A list containing the latex code for the review
        """
        data = list()
        review_start = "\\begin{titlepage}\n" \
                       "\\centering\n" \
                       "{\\scshape\\LARGE HS-RoboTrim2018 \\par}\n" \
                       "\\vspace{1cm}\n" \
                       "{\\scshape\\Large Gjennomgang av sprint \\par}\n" \
                       "Dette dokumentet inneholder gruppas oppsummering av arbeidet som har blitt" \
                       "gjort den foregående sprinten.\n" \
                       "\\vfill\n" \
                       "\\end{titlepage}\n" \
                       "\\begin{center}\n" \
                       "\\bgroup\n" \
                       "\\def\\arraystretch{2}\n" \
                       "\\begin{table}[h!]\n" \
                       "\\begin{tabularx}{\\textwidth}{p{0.32\\textwidth} p{0.32\\textwidth} p{0.32\\textwidth}}\n" \
                       "\\hline\n" \
                       "\\textbf{Dato: } \n" \
                       + str(end_date) + \
                       " & & \\\\\n" \
                       "\\hline\n" \
                       "\\end{tabularx}\n" \
                       "\\end{table}" \
                       "\\begin{longtable}{p{0.20\\textwidth} p{0.15\\textwidth} " \
                       "p{0.10\\textwidth} p{0.10\\textwidth} p{0.35\\textwidth}}\n" \
                       "{\\bf Saks-ID} & {\\bf Ansvarlig} & {\\bf Estimat} & {\\bf Status} & {\\bf Kommentar} \\\\\n" \
                       "\\hline\n"
        data.append(review_start)
        for input in temp_input:
            data.append(input)
        review_end = "\end{longtable}\n" \
                     "\\begin{tabularx}{\\textwidth}{p{0.50\\textwidth} p{0.50\\textwidth}}\n" \
                     "\\hline\n" \
                     "{\\bf Planlagte saker: } " + str(planned_issues) + \
                     " & {\\bf Planlagt estimat: }" + str(planned_estimate) + " \\\\\n" \
                                                                                   "{\\bf Tillagte saker: }" + \
                     str(total_issues - planned_issues) + \
                     " & {\\bf Tillagt estimat: }" + str(total_estimate - planned_estimate) + " \\\\\n" \
                                                                               "\\hline" \
                                                                               "{\\bf Totale saker: }" + \
                     str(total_issues) + \
                     " & {\\bf Totalt estimat: }" + str(total_estimate) + " \\\\\n" \
                                                                               "{\\bf Ufullførte saker: }" + \
                     str(incomplete_issues) + \
                     " & {\\bf Ufullført estimat: }" + str(incomplete_estimate) + " \\\\\n" \
                                                                                       "{\\bf Kansellerte saker: }" + \
                     str(cancelled_issues) + \
                     " & {\\bf Kansellert estimat: }" + str(cancelled_estimate) + " \\\\\n" \
                                                                                       "{\\bf Fullførte saker: }" + \
                     str(complete_issues) + \
                     " & {\\bf Fullført estimat: }" + str(complete_estimate) + " \\\\\n" \
                                                                                    "\\end{tabularx}\n" \
                                                                                    "\\egroup\n" \
                                                                                    "\\end{center}\n"
        data.append(review_end)
        return data

    def make_individual_time_title(self):
        titlepage = "\\begin{titlepage}\n" \
                    "\\centering\n" \
                    "{\\scshape\\LARGE HS-RoboTrim2018 \\par}\n" \
                    "\\vspace{1cm}\n" \
                    "{\\scshape\\Large Individuelle timelister \\par}\n" \
                    "De kommende dokumentene inneholder timelister for alle gruppemedlemmene" \
                    " for den foregående sprinten.\n" \
                    "\\vfill\n" \
                    "\\end{titlepage}\n"
        return titlepage

    def create_individual_time(self, temp_input, name, total_hours):
        """
        Creates individual lists to track hours for each group member for each sprint

        :param temp_input: Contains data from the excel sheet generated from Google Drive
        :param name: Name of the group member
        :param total_hours: Total working hours for the group member during sprint
        :return: A list containing the latex code for the individual time
        """
        data = list()
        overview_start = \
            "\\section*{Individuell timeliste for " + name + "}\n" \
                                                             "\\begin{table}[h!]\n" \
                                                             "\\begin{center}\n" \
                                                             "\\bgroup\n" \
                                                             "\\def\\arraystretch{2}\n" \
                                                             "\\begin{tabular}{l l l r}\n" \
                                                             "\\textbf{Navn: }\n" \
                                                             + name + " & \\textbf{Sprint ID: } \n" \
                                                             "RTS-" + str(self.sprint_id) + " & & \\\\\n" \
                                                                                     "\\end{tabular}\n" \
                                                                                     "\\begin{longtable}{|p{0.15\\textwidth}|c|c|p{0.15\\textwidth}|p{0.4\\textwidth}|}\n" \
                                                                                     "\\hline\n" \
                                                                                     "\\textbf{Dato} & \\textbf{Klokkeslett} & \\textbf{Timer}" \
                                                                                     "& \\textbf{Task ID} & \\textbf{Kommentar} \\\\\n" \
                                                                                     "\\hline\n"
        data.append(overview_start)
        for input in temp_input:
            data.append(input)
        overview_end = "\\hline\n" \
                       "\\end{longtable}\n" \
                       "\\egroup\n" \
                       "\\end{center}\n" \
                       "\\end{table}\n" \
                       "\\indent\\textbf{Total timer: }" + str(total_hours)
        data.append(overview_end)
        return data
