from openpyxl import load_workbook
from ExcelToPython import latex_input
import os
import datetime

"""

CHANGELOG:

14.01.2018:     *   Started coding the script

15.01.2018:     *   Edited the script to support modularity in a bigger degree
                    *   Coded classes for the different tables currently supported:
                        **  SprintPlan
                        **  SprintReview
                        **  IndividualOverview
                        
16.01.2018:     *   Remake of the script in order to facilitate generic support for tables.
                    *   Script is now mainly based in an excel file with sheets, execution is dependent on 
                        sheet names and is more or less the same for each table with different outputs depending
                        on type of table.

20.01.2018:     *   Automatically generates the full report, except the retrospective which is written in the end.
                *   Now counts total working hours of the group, not only individual
                
04.02.2018:     *   Restructured program
                    *   Made own file for the input to Latex in order to get a cleaner code
                    *   Revamped content of Latex file
                        **  Removed includes in order to get one .txt file, done for ease of use because the content
                            has to be copy/pasted to Latex document.
                    *   Revamped issue/estimate counter because of bugs.
                    *   Small fixes to fix output bugs
                    *   Documented code

"""


class TaskHandler:
    task = None

    def create_task(self, sheet_title, row):
        """
        Create a dict for a task

        :param sheet_title: Indicates the type of task to produce
        :param row: Raw data to be sanitized
        :return: Sanitized task
        """
        if sheet_title == 'SprintPlan' or sheet_title == 'SprintPlanNext':
            task = PlanTask(row[0].value, row[1].value, row[2].value, row[3].value).plan_task
        elif sheet_title == 'SprintReview':
            task = ReviewTask(row[0].value, row[1].value, row[2].value, row[3].value, row[4].value).review_task
        elif sheet_title == "SLA" or sheet_title == "PAS" or sheet_title == "OAFR" or \
                sheet_title == "TSV" or sheet_title == "ØJCÅ" or sheet_title == "EH":
            task = IndividualTime(row[0].value, row[1].value, row[2].value, row[3].value, row[4].value).time_list
        return task

    def sort_tasks(self, task_list):
        """
        Sorts task by ID

        :param task_list: Unsorted list of tasks
        :return: Sorted list of tasks
        """
        list_to_sort = list()
        for task in task_list:
            list_to_sort.append(task['id'])
        list_to_sort.sort()
        return list_to_sort

    def get_keys(self, task_list):
        keys = list()
        if task_list.__len__() is not 0:
            for key in task_list[0].keys():
                keys.append(key)
        return keys


class PlanTask:
    plan_task = {
        'id': None,
        'task_name': None,
        'assigned': None,
        'estimate': None
    }

    def __init__(self, id, task_name, assigned, estimate):
        self.plan_task = {
            'id': id,
            'task_name': task_name,
            'assigned': assigned,
            'estimate': estimate
        }


class ReviewTask:
    review_task = {
        'id': None,
        'assigned': None,
        'estimate': None,
        'status': None,
        'comment': None
    }

    def __init__(self, id, assigned, estimate, status, comment):
        self.review_task = {
            'id': id,
            'assigned': assigned,
            'estimate': estimate,
            'status': status,
            'comment': comment
        }


class IndividualTime:
    time_list = {
        'date': None,
        'time': None,
        'hours': None,
        'task_id': None,
        'comments': None
    }

    def __init__(self, date, time, hours, task_id, comments):
        self.time_list = {
            'date': date,
            'time': time,
            'hours': hours,
            'task_id': task_id,
            'comments': comments
        }


class ExcelToLatex:
    task_handler = TaskHandler()
    generated_path = None
    sprint_dir = None
    workbook_name = None
    file_name = None

    sprint_id = None
    start_date = None
    end_date = None
    goal = None

    input = None
    latex_complete = list()
    preamble = list()
    made_individual_title = False

    planned_issues = added_issues = total_issues = complete_issues = \
        incomplete_issues = cancelled_issues = next_planned_issues = 0

    added_estimate = planned_estimate = total_estimate = \
        incomplete_estimate = cancelled_estimate = complete_estimate = \
        next_planned_estimate = total_hours = 0.0

    individual_hours = {
        'SLA': 0.0,
        'PAS': 0.0,
        'OAFR': 0.0,
        'TSV': 0.0,
        'ØJCÅ': 0.0,
        'EH': 0.0
    }

    data = None

    columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']

    def __init__(self, sprint_number):
        self.sprint_id = str(sprint_number)
        self.generated_path = '../Reports/Generated/'
        self.sprint_dir = self.generated_path + '/' + 'RTS-' + self.sprint_id + '/'
        self.workbook_name = 'SprintRapport_RTS-' + self.sprint_id + '.xlsx'
        self.file_name = 'SprintRapport_RTS-' + self.sprint_id
        print('Loading workbook ' + self.workbook_name)
        self.workbook = load_workbook('../Reports/' + self.workbook_name)
        print('Searching for ' + self.generated_path)
        self.check_for_dirs()
        self.input = latex_input.LatexInput(self.sprint_id)

    def check_for_dirs(self):
        """
        Check if there are existing directories for generated files and the current sprint-report
        """
        if not os.path.isdir(self.generated_path):
            print('Could not find dir, creating ' + self.generated_path)
            os.mkdir(self.generated_path)
            print('Created dir for generated reports at ' + self.generated_path)
        print('Checking if dir for RTS-' + self.sprint_id + ' exists')
        if not os.path.isdir(self.sprint_dir):
            print('Could not find dir, creating ' + self.sprint_dir)
            os.mkdir(self.sprint_dir)
            print('Created dir for RTS-' + self.sprint_id + 'at ' + self.sprint_dir)

    def generate_report(self):
        """
        Generate the report based on the data from the current sprint
        """
        txt_file = open(self.sprint_dir + self.file_name + '.txt', 'w')
        tex_file = open(self.sprint_dir + self.file_name + '.tex', 'w')
        tex_file.close()
        for sheet_name in self.workbook.sheetnames:
            sheet = self.workbook.get_sheet_by_name(sheet_name)
            self.excel_to_latex(sheet)
        print('Writing to ' + self.sprint_dir + self.file_name + '.txt')
        print('Generating preamble for report in Latex')
        preamble = self.input.create_report_preamble(self.start_date, self.end_date)
        for line in preamble:
            self.preamble.append(line)
        self.preamble.append('\\newpage\n')
        for line in self.preamble:
            txt_file.write(line)
        for line in self.latex_complete:
            txt_file.write(line)
        txt_file.write('\n\\end{document}')
        print('Report generated!')

    def excel_to_latex(self, sheet):
        """
        Convert the contents of an excel-sheet to latex-code outputted in .txt

        :param sheet: The sheet to be converted
        """
        temp_input = list()
        column_count, row_count = self.get_size(sheet)
        end_column = self.columns[column_count - 2]
        if sheet.title == 'SprintPlan' or sheet.title == 'SprintPlanNext':
            if sheet.title == 'SprintPlan':
                self.start_date = sheet['B1'].value
                self.start_date = self.date_conversion(self.start_date)
            self.read_from_excel(sheet, temp_input, end_column, row_count)
            temp_input = self.input.create_plan(temp_input, self.start_date, self.goal,
                                                self.planned_issues, self.planned_estimate)
            for temp in temp_input:
                self.latex_complete.append(temp)
            self.latex_complete.append('\\newpage\n')
        elif sheet.title == 'SprintReview':
            self.end_date = sheet['B1'].value
            self.end_date = self.date_conversion(self.end_date)
            self.read_from_excel(sheet, temp_input, end_column, row_count)
            temp_input = self.input.create_review(temp_input, self.end_date, self.planned_issues, self.total_issues,
                                                  self. incomplete_issues, self.cancelled_issues, self.complete_issues,
                                                  self.planned_estimate, self.total_estimate, self.incomplete_estimate,
                                                  self.cancelled_estimate, self.complete_estimate)
            for temp in temp_input:
                self.latex_complete.append(temp)
            self.latex_complete.append('\\newpage\n')
        elif sheet.title == 'SLA' or sheet.title == 'PAS' or sheet.title == 'OAFR' or sheet.title == 'TSV' or \
                sheet.title == 'ØJCÅ' or sheet.title == 'EH':
            if self.made_individual_title is not True:
                self.latex_complete.append(self.input.make_individual_time_title())
                self.made_individual_title = True
            self.read_from_excel(sheet, temp_input, end_column, row_count)
            name = self.get_name(sheet.title)
            temp_input = self.input.create_individual_time(temp_input, name, self.individual_hours[sheet.title])
            for temp in temp_input:
                self.latex_complete.append(temp)
            self.latex_complete.append('\\newpage\n')

    def read_from_excel(self, sheet, excel_data, end_column, row_count):
        """
        Read data from an excel sheet and produce a list of tasks deducted from the sheet.
        Passes the list of tasks and a sorted list of IDs to row_to_latex

        :param sheet: Excel-sheet to convert
        :param excel_data: Temporary list used to store data from excel in list format
        :param end_column: Used to define the length of the excel-sheet
        :param row_count: Used to define the length of the excel-sheet
        """
        task_list = list()
        sorted_list = None

        for row in sheet['A5': end_column + str(row_count - 1)]:
            task_list.append(self.task_handler.create_task(sheet.title, row))

        if sheet.title == 'SprintPlan' or sheet.title == 'SprintPlanNext' or sheet.title == 'SprintReview':
            sorted_list = self.task_handler.sort_tasks(task_list)

        # TODO: Count issues
        self.extract_data(sheet.title, task_list)
        keys = self.task_handler.get_keys(task_list)
        self.row_to_latex(sheet.title, sorted_list, task_list, keys, excel_data)

    def row_to_latex(self, sheet_title, sorted_list, task_list, keys, excel_data):
        """
        Appends the received content in the list of tasks to the temporary list of tasks

        :param sheet_title: Title of the sheet to convert
        :param sorted_list: Sorted list of the tasks
        :param task_list: Task list with all data in unsorted form
        :param keys: Keys of the sheet to convert
        :param excel_data: Temporary list to store data for tasks
        """
        if sheet_title == 'SprintPlan' or sheet_title == 'SprintPlanNext' or sheet_title == 'SprintReview':
            for id in sorted_list:
                for task in task_list:
                    if task['id'] == id:
                        excel_data.append(self.get_row(keys, task))
        elif sheet_title == 'SLA' or sheet_title == 'PAS' or sheet_title == 'OAFR' or \
                sheet_title == 'TSV' or sheet_title == 'ØJCÅ' or sheet_title == 'EH':
            for task in task_list:
                excel_data.append(self.get_row(keys, task))

    def get_row(self, keys, task):
        """
        Produce latex-code from the passed task

        :param keys: Keys of the task
        :param task: Data of the task to be converted
        :return:
        """
        counter = 1
        current_row = ""
        for key in keys:
            if counter < keys.__len__():
                current_row = current_row + str(task[key]) + ' & '
            else:
                current_row = current_row + str(task[key]) + ' \\\\\n'
            counter = counter + 1
        return current_row

    def get_size(self, sheet):
        """
        Get the size of the sheet to convert

        :param sheet: The sheet to convert
        :return: Number of columns in the sheet and number of rows
        """
        i = 1
        end = False
        while not end:
            current = sheet.cell(row=4, column=i).value
            if current == "Hend":
                no_of_columns = i
                end = True
            i = i + 1
        i = 1
        end = False
        while not end:
            current = sheet.cell(row=i, column=1).value
            if current == "Vend":
                no_of_rows = i
                end = True
            else:
                i = i + 1
        return no_of_columns, no_of_rows

    def get_name(self, initials):
        """
        Get full name based on initials passed

        :param initials: Initials of participants in the group
        :return: Full name of the person
        """
        names = {
            'SLA': 'Sondre L. Aronsen',
            'PAS': 'Per A. Stadheim',
            'OAFR': 'Ole Anders F. Reistad',
            'TSV': 'Thomas S. Vatle',
            'ØJCÅ': 'Øyvind J. Åslie',
            'EH': 'Erlend Helgerud'
        }
        for name in names.keys():
            if name == initials:
                return names[name]

    def extract_data(self, sheet_title, task_list):
        """
        Extract data for issues and estimates from the passed sheet

        :param sheet_title: Sheet to extract data from
        :param task_list: List of tasks
        """
        if sheet_title == 'SprintPlan':
            for task in task_list:
                self.planned_issues = self.planned_issues + 1
                if task['estimate'] is not None:
                    self.planned_estimate = self.planned_estimate + float(task['estimate'])
        elif sheet_title == 'SprintReview':
            for task in task_list:
                self.total_issues = self.total_issues + 1
                if task['estimate'] is not None:
                    self.total_estimate = self.total_estimate + float(task['estimate'])
                if task['status'] == 'F':
                    self.complete_issues = self.complete_issues + 1
                    if task['estimate'] is not None:
                        self.complete_estimate = self.complete_estimate + float(task['estimate'])
                elif task['status'] == 'K':
                    self.cancelled_issues = self.cancelled_issues + 1
                    if task['estimate'] is not None:
                        self.cancelled_estimate = self.cancelled_estimate + float(task['estimate'])
                else:
                    self.incomplete_issues = self.incomplete_issues + 1
                    if task['estimate'] is not None:
                        self.incomplete_estimate = self.incomplete_estimate + float(task['estimate'])
        elif sheet_title == 'SprintPlanNext':
            for task in task_list:
                self.next_planned_issues = self.next_planned_issues + 1
                if task['estimate'] is not None:
                    self.next_planned_estimate = self.next_planned_estimate + float(task['estimate'])
        elif sheet_title == 'SLA' or sheet_title == 'PAS' or sheet_title == 'OAFR' or sheet_title == 'TSV' or \
                sheet_title == 'ØJCÅ' or sheet_title == 'EH':
            for task in task_list:
                if task['hours'] is not None:
                    self.individual_hours[sheet_title] = self.individual_hours[sheet_title] + float(task['hours'])
                    self.total_hours = self.total_hours + float(task['hours'])

    def date_conversion(self, date):
        """
        Convert from datetime.datetime to used format

        :param date: Date which is checked for format, if datetime.datetime gets converted
        :return: Date in correct format
        """
        if isinstance(date, datetime.datetime):
            if str(date.day).__len__() == 1:
                day = '0' + str(date.day)
            else:
                day = str(date.day)
            if str(date.month).__len__() == 1:
                month = '0' + str(date.month)
            else:
                month = date.month
            date_to_return = day + '.' + month + '.' + str(date.year)
            return date_to_return
        else:
            return date


e2l = ExcelToLatex(4)
e2l.generate_report()
